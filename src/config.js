import Raven from 'raven-js';
const {
  REACT_APP_ENV,
  REACT_APP_SHOW_LOGGER,
  REACT_APP_DEBUG_ENABLED,
  REACT_APP_API_BASE_URL
} = process.env;

if (REACT_APP_ENV === 'production') {
  Raven
    .config('https://bce7def7c36241ae8822d5e284bb0ee1@sentry.io/261204')
    .install();
}

export const API_BASE_URL = REACT_APP_API_BASE_URL;
export const REDUX_LOGGER_ENABLED = REACT_APP_SHOW_LOGGER === 'true';
export const DEBUG_ENABLED = REACT_APP_DEBUG_ENABLED === 'true';

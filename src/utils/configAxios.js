import _ from 'lodash';
import axios from 'axios';
import { store } from './configStore';
import Moment from 'moment';

import { setUserAuthenticated } from 'app/reducers/userReducer';

const instance = axios.create({
  baseURL: '/api/v1'
});

instance.interceptors.response.use(
  (response) => {
    // our api returns results in a `data` key & axios wraps the response in a `data` key also
    const { data } = response.data;
    const url = _.get(response, 'request.responseURL');
    const isAuth = url && url.indexOf('/authenticate') !== -1;

    if (isAuth && data.token) {
      // signal to the app we logged in
      store.dispatch(setUserAuthenticated({
        isAuthenticated: true,
        token: data.token,
        expires: Moment().add(1, 'hours').toISOString()
      }));
    }

    // let the response pass through to the service
    return response;
  },
  (error) => {
    const { response } = error;

    if (response && response.status === 401) {
      store.dispatch(setUserAuthenticated({
        isAuthenticated: false
      }));
    }

    return Promise.reject(error);
  }
);

const publicRoutes = [
  '/authenticate',
  '/password/reset',
  '/password/confirm'
];

function checkPublicRoutes(url) {
  return publicRoutes.find(partial => url.indexOf(partial) !== -1);
}

instance.interceptors.request.use(
  (config) => {
    const isPublic = checkPublicRoutes(config.url);

    if (!isPublic && !config.headers.Authorization) {
      const { user } = store.getState();
      config.headers.Authorization = `Bearer ${user.token}`;
    }

    return config;
  },
  (error) => Promise.reject(error)
);

export default instance;

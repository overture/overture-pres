import { applyMiddleware, compose, createStore } from 'redux';
import { persistReducer, persistStore } from 'redux-persist';
import storage from 'redux-persist/es/storage';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';
import { reducers } from '../app/reducers';

const reducer = persistReducer({
  key: 'root',
  storage,
  whitelist: ['user', 'gamesCache']
}, reducers);
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
let enhancers = composeEnhancers(applyMiddleware(thunk));
let logger;

if (process.env.REACT_APP_SHOW_LOGGER === 'true') {
  logger = createLogger({
    collapsed: true
  });
}

if (logger) {
  enhancers = composeEnhancers(applyMiddleware(thunk, logger));
}

export const store = createStore(
  reducer, {}, enhancers
);

export const persistor = persistStore(store);

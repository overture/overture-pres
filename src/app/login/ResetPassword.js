import * as React from 'react';
import { Link } from 'react-router-dom';

export default (props) => {
  return (
    <div className="container">
      <div className="grid-x align-center">
        <form className="medium-4 cell">
          <label>
            Password
            <input type="password" placeholder="new password" />
          </label>
          <label>
            Confirm Password
            <input type="password" placeholder="confirm password" />
          </label>
          <div className="menu">
            <Link className="button" to="/forgot">Reset Passeword</Link>
          </div>
        </form>
      </div>
    </div>
  );
};

import * as React from 'react';
import { Link } from 'react-router-dom';

export default (props) => (
  <div className="container">
    <div className="grid-container">
      <form className="medium-4 cell">
        <div className="grid-x">
          <div className="medium-6 cell">
            <label>
              First Name
              <input type="text" />
            </label>
          </div>
          <div className="medium-6 cell">
            <label>
              Last Name
              <input type="text" />
            </label>
          </div>
        </div>
        <label>
          Zip Code
          <input type="text" required={true} />
          <span className="help-text">Why do we require this? </span>
          <span className="help-text">
            When looking for players, groups, and events we need to know generally where to look
          </span>
        </label>
        <label>
          Email
          <input type="email" placeholder="example@example.com" />
        </label>
        <label>
          Password
          <input type="password" placeholder="6 character minimum" />
        </label>
        <label>
          Confirm Password
          <input type="password" placeholder="confirm" />
        </label>
        <label>
          Weenie The Poo
          <input type="text" name="poo" placeholder="pot o' gold" />
        </label>
        <ul className="menu simple">
          <li>
            <button className="button">Signup</button>
          </li>
          <li>
            Already have an account? <Link to="/login">Login</Link>
          </li>
        </ul>
      </form>
    </div>
  </div>
);

import React from 'react';
import { Link } from 'react-router-dom';

export default (props) => {
  return (
    <div className="container">
      <div className="grid-x align-center">
        <form className="medium-4 cell">
          <p>We'll send you an raven with a magical inscription granting you access back into our realm.</p>
          <label>
            Email
            <input type="email" placeholder="example@example.com" />
          </label>
          <div className="menu">
            <Link className="button" to="/forgot">Reset Passeword</Link>
            <Link to="/login">cancel</Link>
          </div>
        </form>
      </div>
    </div>
  );
};

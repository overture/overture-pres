import React from 'react';

import { store } from 'utils/configStore';
import { logout } from 'app/reducers/userReducer';

class Logout extends React.Component {
  componentDidMount() {
    // this action needs to come after persist/REHYDRATE in the event loop
    setTimeout(() => {
      store.dispatch(logout());
    }, 0);
  }

  render() {
    return (
      <div className="container">
        <div className="grid-container">
          <div className="grid-x align-center align-bottom">
            You have been successfully logged out
          </div>
        </div>
      </div>
    );
  }
}

export default Logout;

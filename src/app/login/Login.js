import _ from 'lodash';
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { DEBUG_ENABLED } from '../../config';
import { RequestLogin } from '../services';

class Login extends React.Component {
  componentDidUpdate(prevProps) {
    const { props } = this;
    const prev = _.get(props, 'location.state.from');
    const location = prev || '/dashboard';

    // if the user was not previously auth'd but now is...
    if (props.user.isAuthenticated) {
      props.history.replace(location);
    }
  }

  render() {
    return (
      <div className="container">
        <div className="grid-container">
          <div className="grid-x align-center align-bottom">
            <form className="cell medium-4" onSubmit={this.handleSubmit}>
              <label>
                Email
                <input type="email" name="email" placeholder="example@example.com" />
              </label>
              <label>
                Password
                <input type="password" name="password" />
              </label>
              {DEBUG_ENABLED && (
                <select>
                  <option value="">Trevor Pierce (super)</option>
                  <option value="">Mathew (game owner)</option>
                  <option value="">Sophie (player)</option>
                </select>
              )}
              <button className="button">Login</button>
              <ul className="menu simple">
                <li>
                  <Link to="/forgot">Forgot Password</Link>
                </li>
                <li>
                  <Link to="/signup">Signup</Link>
                </li>
              </ul>
            </form>
          </div>
        </div>
      </div>
    );
  }

  handleSubmit = (event) => {
    event.preventDefault();
    const { elements } = event.target;

    this.props.RequestLogin({
      email: elements.email.value,
      password: elements.password.value
    });
  }
}

const mapStateToProps = ({ user }) => ({
  user
});

const mapDispatchToProps = (dispatch) => (
  bindActionCreators({ RequestLogin }, dispatch)
);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);

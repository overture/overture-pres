import { SET_AUTHENTICATED, SET_USER } from './';

export const setUserAuthenticated = (payload) => ({
  type: SET_AUTHENTICATED,
  payload
});

export const setUser = (payload) => ({
  type: SET_USER,
  payload
});

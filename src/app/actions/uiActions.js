import {
  SET_LOADING,
  UPDATE_MAIN_NAV,
  UPDATE_GLOBAL_MODALS,
  UPDATE_CONTEXT_NAV
} from './';

export const setLoading = () => ({
  type: SET_LOADING
});

export const updateMainNav = payload => ({
  type: UPDATE_MAIN_NAV,
  payload
});

export const updateGlobalModals = payload => ({
  type: UPDATE_GLOBAL_MODALS,
  payload
});

export const updateContextNav = payload => ({
  type: UPDATE_CONTEXT_NAV,
  payload
});

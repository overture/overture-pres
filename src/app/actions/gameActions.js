import {
  SET_GAME_LISTING_FILTERS,
  CLEAR_GAME_LISTING_FILTERS
} from './';

/* @Todo: The following 2 can probably be abstracted to just set/clear filters */
export const setGamesFilters = (payload) => ({
  type: SET_GAME_LISTING_FILTERS,
  payload
});

export const clearGamesFilters = (payload) => ({
  type: CLEAR_GAME_LISTING_FILTERS,
  payload
});

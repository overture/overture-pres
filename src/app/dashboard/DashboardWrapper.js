import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link, Route, Switch } from 'react-router-dom';
import Loadable from 'react-loadable';
import classnames from 'classnames';
import Moment from 'moment';

// utils
import { routeFactory } from '../../utils/routeUtils';

import { CreateEventWizard } from 'app/dashboard/events';
import { PlayModal } from 'app/dashboard/games';
import { MainMenu } from 'app/dashboard/global';
import { updateMainNav } from 'app/actions/uiActions';
import { setUserAuthenticated } from 'app/reducers/userReducer';

import Dashboardindex from 'app/dashboard/DashboardIndex';
import EventsIndex from 'app/dashboard/events/EventsIndex';
import UserDetails from 'app/dashboard/users/UserDetails';

import gameRoutes from './games/game.routes';

const DASHBOARD_ROUTES = [
  ...gameRoutes
];

class DashboardWrapper extends React.Component {
  previousLocation = this.props.location;

  componentWillMount() {
    const { props } = this;
    const { user } = props;

    if (user.expires && Moment().isSameOrAfter(user.expires)) {
      props.history.push('/login');
      setUserAuthenticated({
        isAuthenticated: false,
        token: '',
        expires: ''
      });
    }
  }

  componentWillReceiveProps() {
    const { props } = this;
    const { user } = props;

    // check if user is authenticated or their "session" expired
    if (
      !user.isAuthenticated ||
      !user.expires ||
      Moment().isSameOrAfter(user.expires)
    ) {
      props.history.push('/login');
    }
  }

  componentWillUpdate(nextProps) {
    const { location } = this.props;
    // if props.location isn't a modal, set previous location to incoming / new location
    if (
      nextProps.history.action !== 'POP' &&
      (!location.state || !location.state.modal)
    ) {
      this.previousLocation = this.props.location;
    }
  }

  render() {
    const { props } = this;
    const { location } = props;
    const isModal = !!(
      location.state &&
      location.state.modal
    );

    const appContainerClasses = classnames(
      'app-container',
      {
        'menu-is-open': props.mainnav.isOpen,
        'modal-is-open': isModal
      }
    );
    const containerClasses = classnames(
      'top-bar',
      { 'menu-is-open': props.mainnav.isOpen }
    );

    return ([
      <nav className={containerClasses} key="top-bar">
        <span>
          <button className="nav-toggle" onClick={() => {
            props.updateMainNav({ isOpen: !props.mainnav.isOpen });
          }}>
            <i className="fa fa-list-ul" />
          </button>
        </span>
        <span>
          <Link className="top-bar-left" to="/dashboard" >
            <img src="http://placehold.it/25" alt="logo" />
          </Link>
        </span>
        <span>??</span>
      </nav>,
      <div id="app-container" className={appContainerClasses} key="main-content">
        <MainMenu className="main-menu" history={props.history} />
        <main id="main-content" className='main-content-container'>
          <div className="wrapper">
            <Switch location={isModal ? this.previousLocation : location}>
              {routeFactory(DASHBOARD_ROUTES)}
              <Route exact path={`${props.match.url}/events`} component={EventsIndex} />
              <Route path={`${props.match.url}/${props.user._id}`} component={UserDetails} />
              <Route path={`${props.match.url}/${props.user._id}/groups`} component={() => <div>My Groups</div>} />
              <Route exact path={`${props.match.url}/log-play`} component={PlayModal} />
              <Route exact path={`${props.match.url}`} component={Dashboardindex} />
              <Route path="*" component={() => 'Dashboard Not Found'} />
            </Switch>
            {isModal ? <Route path={`${props.match.url}/events/create`} component={CreateEventWizard} /> : null}
          </div>
        </main>
        <LoadableContextMenu
          className="context-menu"
          isOpen={props.contextNav.isOpen}
        />
      </div>
    ]);
  }
}

const LoadableContextMenu = Loadable({
  loader: () => import('../dashboard/global/context-menu/ContextMenu'),
  loading: () => 'loadable context menu is loading...'
});

const mapStateToProps = ({ ui, user }) => ({
  mainnav: ui.mainnav,
  contextNav: ui.contextNav,
  user
});

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    updateMainNav,
    setUserAuthenticated
  }, dispatch)
);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DashboardWrapper);

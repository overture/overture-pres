import React from 'react';
import { connect } from 'react-redux';

export const UserDetails = ({ user }) => (
  <div>
    <header style={{
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    }}>
      <img src={user.avatar} alt={user.username} style={{ borderRadius: '50%' }} />
      <div>{user.username}</div>
      <h1>{user.first_name} {user.last_name}</h1>
      <button className="button">Contact {user.first_name}</button>
    </header>
    <section>
      <h2>Recently Played</h2>
      <ul className="grid-x grid-margin-x no-bullet">
        <li className="cell medium-2">
          <img src="https://placehold.it/100" alt="fake img" />
          <h6>Game 1</h6>
          won
        </li>
        <li className="cell medium-2">
          <img src="https://placehold.it/100" alt="fake img" />
          <h6>Game 2</h6>
          lost
        </li>
        <li className="cell medium-2">
          <img src="https://placehold.it/100" alt="fake img" />
          <h6>Game 3</h6>
          won
        </li>
        <li className="cell medium-2">
          <img src="https://placehold.it/100" alt="fake img" />
          <h6>Game 4</h6>
          won
        </li>
        <li className="cell medium-2">
          <img src="https://placehold.it/100" alt="fake img" />
          <h6>Game 5</h6>
          lost
        </li>
      </ul>
    </section>
    <div className="grid-x grid-margin-x">
      <section className="cell medium-6">
        <h2>Favorite Games</h2>
      </section>
      <section className="cell medium-6">
        <h2>Achievements</h2>
      </section>
    </div>
  </div>
);

const mapStateToProps = ({ user }) => ({
  user
});

export default connect(
  mapStateToProps
)(UserDetails);

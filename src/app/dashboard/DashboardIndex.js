import React from 'react';

export default () => (
  <div>
    <h1 className="signika primary-heading">Board Games</h1>
    <h3 className="panel secondary-heading--caps">Hand-Management</h3>
    <p>
      Lorem ipsum dolor, sit amet consectetur adipisicing elit.
      Adipisci nihil earum porro consequatur illum laborum non cum labore? Sunt nesciunt
      ipsum exercitationem ipsam minima error magni esse nobis? Consequatur, reprehenderit.
    </p>
    <h3 className="panel secondary-heading--caps">General Information</h3>
    <p>
      Lorem ipsum dolor, sit amet consectetur adipisicing elit.
      Adipisci nihil earum porro consequatur illum laborum non cum labore?
    </p>
    <label>
      Game Name
      <input type="text" />
    </label>
    <label>
      Expansion Name
      <input type="text" />
    </label>
    <label>
      Min Player Count
      <input type="text" />
    </label>
    <h3 className="panel secondary-heading--caps">More Information</h3>
    <p>
      Lorem ipsum dolor, sit amet consectetur adipisicing elit.
      Adipisci nihil earum porro consequatur illum laborum non cum labore?
    </p>
    <p>
      Lorem ipsum dolor sit amet consectetur adipisicing elit.
      Non, eligendi fugit ratione eveniet nesciunt ea nostrum nisi debitis nihil reprehenderit provident?
      Placeat explicabo animi maiores quasi cupiditate sit possimus magnam?
    </p>
    <p>
      Lorem ipsum dolor sit, amet consectetur adipisicing elit.
      At reprehenderit culpa dignissimos dolorum quasi voluptatem.
      Facere quo architecto quos error, eius earum. Vitae sunt veniam tenetur sequi quas maxime incidunt.
      Lorem ipsum dolor, sit amet consectetur adipisicing elit.
      Adipisci nihil earum porro consequatur illum laborum non cum labore?
    </p>
  </div>
);

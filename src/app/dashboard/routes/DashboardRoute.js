import React from 'react';
import { connect } from 'react-redux';
import { Redirect, Route, withRouter } from 'react-router-dom';

const DashboardRoute = ({
  component: Component,
  isAuthenticated,
  ...rest
 }) => (
    <Route
      {...rest}
      render={(props) => (
        isAuthenticated ? (
          <Component {...props} />
        ) : (
            <Redirect
              to={{
                pathname: '/login',
                state: { from: props.location }
              }}
            />
          )
      )}
    />
  );

const mapStateToProps = ({ user }) => {
  return {
    isAuthenticated: user.isAuthenticated
  };
};

export default withRouter(
  connect(mapStateToProps)(DashboardRoute)
);

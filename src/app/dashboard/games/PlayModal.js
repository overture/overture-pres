import React from 'react';
import { Link } from 'react-router-dom';
import { Async } from 'react-select';
import 'react-select/dist/react-select.css';
import _ from 'lodash';

import { Modal } from '../../../components/modals';
import { PlayModalComp, PlayModalCoop } from './';

import { RequestGameList } from '../../services';
import { PostPlay } from '../../services';
import { availableGameInteractions } from '../../data/gameData';

class PlayModal extends React.Component {
  LogPlayForm;
  state = {
    isOpen: true,
    playerInteraction: {
      label: '',
      value: ''
    },
    selectedGame: {
      label: '',
      value: '',
      player_interactions: []
    },
    formData: {}
  };

  render() {
    const { state } = this;
    const selectedGameValue = state.selectedGame && state.selectedGame.value;

    return (
      <Modal
        contentLabel="bleh"
        isOpen={state.isOpen}
        onRequestClose={this.onRequestClose}
      >
        <h2>Log Game Play</h2>
        <p>
          By logging play you keep a track record of your plays and enable
          Overture to become more accurate when determining game play times.
        </p>
        <form onSubmit={this.logPlay}>
          <label>
            What did you play?
            <Async
              name="game"
              onChange={this.handleGameChange}
              loadOptions={this.searchGames}
              value={selectedGameValue}
            />
          </label>
          <p>Player Interaction: {this.getPlayerInteractionText()}</p>
          <p>Team Play: No</p>
          <div className="grid-x grid-margin-x">
            <div className="cell small-6">
              <label>
                Date played
                <input
                  name="date_played"
                  onInput={this.handleInput}
                  type="date"
                />
              </label>
            </div>
            <div className="cell small-6">
              <label>
                Duration
                <input
                  name="duration"
                  onInput={this.handleInput}
                  type="number"
                />
                <p className="help-text">minutes</p>
              </label>
            </div>
          </div>
          {/* {props.currentEvent.players.map((player: any, i: number) => {
            return <span key={`comp-player-${i}`}>{player.fullname}</span>;
          })} */}
          <hr />
          <h3>Scoring Details</h3>
          {this.getAdditionalFields(state.playerInteraction.value)}
          <hr />
          <div className="menu align-right">
            <Link to="/dashboard">Cancel</Link>
            <button type="submit" className="button">Log Play</button>
          </div>
        </form>
      </Modal>
    );
  }

  getAdditionalFields(playerInteraction) {
    const { currentEvent } = this.props;
    let players = [];

    players = [{
      full_name: 'Trevor Pierce',
      _id: 1
    }, {
      full_name: 'Mark Hagood',
      _id: 2
    }];

    if (currentEvent) {
      players = currentEvent.players;
    }

    switch (playerInteraction) {
      case 'coop':
        return <PlayModalCoop players={players} />;

      case 'competitive':
        return <PlayModalComp formData={this.state.formData} players={players} />;

      default:
        return null;
    }
  }

  getPlayerInteractionText() {
    const { state } = this;

    if (state.selectedGame && state.selectedGame.player_interactions.length > 1) {
      return (
        <span>
          {state.playerInteraction.label} <button>change</button>
        </span>
      );
    } else {
      return <span>{state.playerInteraction.label}</span>;
    }
  }

  handleGameChange = (selectedGame: any) => {
    let playerInteraction = { label: '', value: '' };

    if (selectedGame) {
      playerInteraction = (
        _.find(
          availableGameInteractions,
          (i: any) => i.value === selectedGame.player_interactions[0]
        )
      );
    }

    this.setState({
      selectedGame,
      playerInteraction,
      formData: {
        ...this.state.formData,
        game: selectedGame.value,
        kind: playerInteraction.value
      }
    });
  }

  logPlay = (e: any) => {
    e.preventDefault();

    PostPlay(this.state.formData)
      .catch((error: any) => {
        console.log('error posting play: ', error);
      });
  }

  searchGames() {
    return (
      RequestGameList()
        .then((games: any) => {
          const options = games.map((game: any) => ({
            label: game.name,
            value: game._id,
            player_interactions: game.player_interaction
          }));

          return { options };
        })
    );
  }

  handleInput = (e: any) => {
    const input = e.target;
    this.setState({
      formData: { ...this.state.formData, ...{ [input.name]: input.value } }
    });
  }

  onRequestClose = () => {
    this.setState({ isOpen: false });
    const { props } = this;
    const prev = _.get(props, 'location.state.from');
    const location = prev || '/dashboard';

    console.log(props);
    console.log(location);

    props.history.replace(location);
  }
}

export default PlayModal;

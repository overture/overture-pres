import React from 'react';
// import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import _ from 'lodash';

import { RequestGame } from '../../services';

class GameDetails extends React.Component {
  state = {
    game: {}
  };

  componentDidMount() {
    const { gameid } = this.props.match.params;

    RequestGame(gameid)
      .then((game) => this.setState({ game }))
      .catch((error) => {
        console.log('Failed to find game', error);
      });
  }

  render() {
    const { state } = this;

    if (_.isEmpty(state.game)) {
      return <div>No Game Found</div>;
    }

    return <h1 className="primary-heading">{state.game.name}</h1>;
  }
}

// const mapDispatchToProps = (dispatch: any) => (
//   bindActionCreators({ RequestGame }, dispatch)
// );

export default connect(
  null
)(GameDetails);

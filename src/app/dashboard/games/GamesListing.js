import { isEqual } from 'lodash';
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, compose } from 'redux';
import withRouter from 'components/hocs/withRouter';

import { updateContextNav } from 'app/actions/uiActions';
import { getGamesCache, updateGamesCache } from './reducers/gamesCache.reducer';
import { RequestGameList } from 'app/services';

import GameList from 'app/dashboard/games/games-listing/GameList';

const initialState = {
  errors: [],
  games: [],
  meta: {
    total: 0
  },
  isLoading: false,
};

class GameListing extends React.PureComponent {
  state = initialState

  componentDidMount() {
    const { props } = this;
    const { cache } = props;

    // check if we have any games in cache
    if (cache.data.length) {
      this.setState({
        games: cache.data,
        meta: cache.meta,
      });
      return;
    }

    if (cache.query) {
      this.makeRequest(props.location.query);
      return;
    }

    this.makeRequest(props.location.query);
  }

  componentDidUpdate(prevProps) {
    const { props } = this;

    if (!isEqual(prevProps.location.query, props.location.query)) {
      this.makeRequest(props.location.query);
    }
  }

  render() {
    const { props, state } = this;

    return (
      <div className="game-list">
        <div className="action-bar game-search">
          <input type="text" placeholder="Search Games" />
          <button onClick={() => props.updateContextNav({ isOpen: true })}>
            <i className="fa fa-sliders" />
          </button>
        </div>
        <GameList {...state} query={props.location.query} />
      </div>
    );
  }

  makeRequest(params) {
    this.setState({ isLoading: true });

    return (
      RequestGameList(params)
        .then(({data: { data, meta }}) => {
          this.setState({ games: data, meta, isLoading: false });
          this.props.updateGamesCache({
            data,
            query: params,
            meta: {
              total: meta.total,
            },
          });
        })
        .catch(error => {
          // should only be catching network errors
          this.setState({ isLoading: false });
          console.log(error);
        })
    );
  }
}

GameListing.defaultProps = {
  cache: {
    data: [],
    query: null,
    meta: {
      total: 0,
    },
  },
};

const mapStateTopProps = state => ({
  cache: getGamesCache(state),
});

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    updateContextNav,
    updateGamesCache,
  }, dispatch)
);

export default compose(
  withRouter,
  connect(
    mapStateTopProps,
    mapDispatchToProps,
  ),
)(GameListing);

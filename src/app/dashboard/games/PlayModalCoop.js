import React from 'react';

class PlayModalCoop extends React.PureComponent {
  render() {
    // const { props } = this;

    return (
      [
        (
          <div key="coop-winners" className="cell large-6">
            <fieldset>
              <legend>Did you win?</legend>
              <input id="win-yes" type="radio" name="coop-win" value="yes" /><label htmlFor="win-yes">Yes</label>
              <input id="win-no" type="radio" name="coop-win" value="no" /><label htmlFor="win-no">No</label>
            </fieldset>
          </div>
        )
      ]
    );
  }
}

export default PlayModalCoop;

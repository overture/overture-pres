import React from 'react';
import { Link } from 'react-router-dom';
import { CircleActions } from 'components'

class GameCard extends React.PureComponent {
  state = {
    actionsMenuOpen: false
  };

  render() {
    const { props } = this;
    const attr = props.game.attributes;

    return (
      <div className="card--game">
        <figure>
          <Link to={`/dashboard/games/${attr.slug}`}>
            <img src="https://placehold.it/400x250" alt="" />
          </Link>
        </figure>
        <div className="card__content">
          <span>{attr.primary_mechanic || ''}</span>
          <h6 className="card__title">{attr.name}</h6>
          <p>{attr.description || 'Spooky scarey skeletons are doing things in their sleep...'}</p>
          <ul className="no-bullet">
            <li>{this.formatTimeText(attr.min_duration, attr.max_duration)}</li>
            <li>{this.formatPlayerText(attr.player_min, attr.player_max)}</li>
          </ul>
        </div>
        <CircleActions className="card__circle-actions">
          <button disabled><i className="fa fa-list-alt" /></button>
          <button><i className="fa fa-heart-o" /></button>
          <button><i className="fa fa-star-o" /></button>
        </CircleActions>
      </div>
    );
  }

  formatTimeText(minDuration, maxDuration) {
    return (minDuration === maxDuration)
      ? `~${minDuration} min`
      : `${minDuration} - ${maxDuration} min`;
  }

  formatPlayerText(playerMin, playerMax) {
    if (!playerMax) {
      return `${playerMin} - unlimited players`;
    }

    if (playerMax === 1) {
      return 'Single Player';
    }

    return (playerMin === playerMax)
      ? `${playerMin} player`
      : `${playerMin} - ${playerMax} players`;
  }
}

export default GameCard;

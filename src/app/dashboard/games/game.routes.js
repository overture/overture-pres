import Loadable from 'react-loadable';

const GameListing = Loadable({
  loader: () => import('./GamesListing'),
  loading: () => 'Loading Game Listing...',
});

const GameDetails = Loadable({
  loader: () => import('./GameDetails'),
  loading: () => 'Loading Game Details...',
});

export default [
  {
    path: '/dashboard/games',
    exact: true,
    component: GameListing
  },
  {
    path: '/dashboard/games/:gameid',
    component: GameDetails
  },
];

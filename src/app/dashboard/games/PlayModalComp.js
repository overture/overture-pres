import React from 'react';
import _ from 'lodash';

// import {
//   PlayerScore,
//   PlayLogFormData
// } from './PlayModal';

class PlayModalComp extends React.PureComponent {
  render() {
    const { props } = this;

    return (
      [
        (
          <div key="comp-winners" className="cell large-6">
            {/* <label>
              Who won?
              <input type="text" placeholder="Search by game name" />
            </label> */}
          </div>
        ),
        (
          <ul className="no-bullet play-log__players" key="comp-player-stats">
            {props.players.map((player, i) => {
              return (
                <li key={`player-${i}`}>
                  <button className="flex-container align-justify">
                    {player.full_name}
                    <span>
                      {this.displayPlayerScore(props.formData.scores, player._id)}
                      <i className="fa fa-angle-down" />
                    </span>
                  </button>
                  <div>
                    <label className="flex-container align-justify">
                      First Play <input type="checkbox" value="yes" />
                    </label>
                    <div className="flex-container align-justify align-middle">
                      <label className="text-right middle" htmlFor={`${player.full_name}-score`}>Score</label>
                      <input
                        type="number"
                        id={`${player.full_name}-score`}
                        name={`${player.full_name}-score`}
                      />
                    </div>
                    <button className="text-right link alert">Remove Player</button>
                  </div>
                </li>
              );
            })}
            <li>
              <button>Add Player <i className="fa fa-plus" /></button>
            </li>
          </ul>
        )
      ]
    );
  }

  displayPlayerScore(scores, playerId) {
    const matchedScore = _.find(scores, ({ player_id }) => playerId === player_id);
    return (matchedScore) ? matchedScore.score : 0;
  }
}

export default PlayModalComp;

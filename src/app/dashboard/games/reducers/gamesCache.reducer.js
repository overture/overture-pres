/* action types */
const UPDATE_GAME_CACHE = 'UPDATE_GAME_CACHE';

/* selectors */
export function getGamesCache(state) {
  return state.gamesCache;
}

/* actions */
export function updateGamesCache(payload) {
  return {
    type: UPDATE_GAME_CACHE,
    payload,
  }
}

/* reducer */
const initialState = {
  data: [],
  query: {},
  meta: {
    total: 0,
  },
};
export default (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_GAME_CACHE:
      return {
        ...state,
        ...action.payload,
      };

    default:
      return state;
  }
};

import React from 'react';
import GameCard from 'app/dashboard/games/GameCard';

export default props => {
  if (props.isLoading) {
    return <p>Looking for games...</p>;
  }

  if (props.errors.length) {
    // @TODO notify engineering via sentry
    return <p>Hmmm, we seem to be experiencing an issue. We've notified the engineering team. Please check back later.</p>;
  }

  if (!props.games || !props.games.length) {
    return (
      <div>
        <p>We looked for games using the following filters but didn't find any. Please modify your selections.</p>
        <ul className="no-bullet">
          {/* @TODO make filters output look better */}
          {Object.keys(props.query).map(key => (
            <li key={key}>{`${key}: ${props.query[key]}`}</li>
          ))}
        </ul>
      </div>
    );
  }

  return ([
    <p key="pagination">Showing {props.games.length} of {props.meta.total}</p>,
    <ul className="grid-x grid-margin-x" key="game-list">
      {props.games.map(game => (
        <li className="cell medium-4 large-3" key={`game-${game.id}`}>
          <GameCard game={game} />
        </li>
      ))}
    </ul>
  ]);
}

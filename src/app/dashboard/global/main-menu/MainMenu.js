import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { updateMainNav, updateGlobalModals } from 'app/actions/uiActions';

import MenuLink from 'app/dashboard/global/sidebar/MenuLink';

class MainMenu extends React.PureComponent {
  render() {
    const { props } = this;
    const fullName = `${props.user.first_name} ${props.user.last_name}`;

    console.log(props);

    return (
      <aside className={props.className}>
        <footer className="user-pane">
          <MenuLink to={`/dashboard/${props.user._id}`}>
            <img src={props.user.avatar} alt={fullName} /> {fullName}
          </MenuLink>
        </footer>
        <nav>
          <ul className="main-menu__items">
            <li>
              <MenuLink to={`/dashboard/games`}>Games</MenuLink>
            </li>
            <li>
              <MenuLink to={`/dashboard/games?filter[owned_by]=${props.user._id}`}>My Collection</MenuLink>
            </li>
            <li><MenuLink to="/dashboard/log-play">Log Play</MenuLink></li>
            <li>
              <MenuLink to={{
                pathname: '/dashboard/events/create',
                state: { modal: true }
              }}>
                Create Event
              </MenuLink>
            </li>
            <li><MenuLink to="/dashboard/events?location=75071">Public Events</MenuLink></li>
            <li><MenuLink to={`/dashboard/${props.user._id}/groups`}>My Groups</MenuLink></li>
            <li><MenuLink to="/dashboard/lfg">Looking for Group</MenuLink></li>
          </ul>
        </nav>
      </aside>
    );
  }
}

const mapStateToProps = ({ ui, user }) => ({
  ...ui.mainnav,
  user
});

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    updateMainNav,
    updateGlobalModals
  }, dispatch)
);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MainMenu);

import React from 'react';
import classnames from 'classnames';
import { connect } from 'react-redux';
import { bindActionCreators, compose } from 'redux';
import withRouter from 'components/hocs/withRouter';

import { updateContextNav } from 'app/actions/uiActions';
import GameListingContextMenu from './GameListingContextMenu';

class ContextMenu extends React.PureComponent {
  $app = document.getElementById('app-container')

  componentWillMount() {
    console.log('Selector: ', this.$app);
    console.log('$app: ', document.getElementById('app-container'));

    this.$app.addEventListener('click', this.showHandler.bind(this));
    this.$app.addEventListener('keyup', this.showHandler.bind(this));
  }

  componentWillUnmount() {
    this.$app.removeEventListener('click', this.showHandler);
    this.$app.removeEventListener('keyup', this.showHandler);
  }

  render() {
    const { props } = this;
    const containerClasses = classnames(
      props.className,
      { 'is-open': props.isOpen }
    )

    return (
      <aside
        id="context-nav"
        className={containerClasses}
        ref={elem => this.$contextNav = elem}
      >
        <GameListingContextMenu />
      </aside>
    );
  }

  showHandler(e) {
    const { props } = this;

    if (!props.contextNav.isOpen) {
      return;
    }

    if (e.which === 27) {
      props.updateContextNav({ isOpen: false });
      return;
    }

    if (!hasParent(e.target, 'context-nav')) {
      props.updateContextNav({ isOpen: false });
    }
  }
}

function hasParent(elem, target) {
  if (!elem) {
    return false;
  }

  if (elem.id === target) {
    return true;
  }

  return hasParent(elem.parentElement, target);
}

const mapStateToProps = ({ ui }) => ({
  contextNav: ui.contextNav,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  updateContextNav
}, dispatch);

export default compose(
  withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(ContextMenu);

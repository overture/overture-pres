import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { updateMainNav } from '../../../actions/uiActions';
import MenuLink from './MenuLink';

import SidebarContent from './SidebarContent';

let touchDirection = null;
let touchPosition = 0;
let touchStart = 0;

class Sidebar extends React.Component {
  componentDidMount() {
    // We need to check if we are mobile or not
    if (true) {
      const $mainContent = document.getElementById('main-content');

      $mainContent.addEventListener('touchstart', this.handleTouchStart);
      $mainContent.addEventListener('touchmove', this.handleTouchMove);
      $mainContent.addEventListener('touchend', this.handleTouchEnd);
    }
  }

  componentWillUnmount() {
    const $mainContent = document.getElementById('main-content');

    $mainContent.removeEventListener('touchstart', this.handleTouchStart);
    $mainContent.removeEventListener('touchmove', this.handleTouchMove);
    $mainContent.removeEventListener('touchend', this.handleTouchEnd);
  }

  render() {
    const { props } = this;
    const fullName = `${props.first_name} ${props.last_name}`;

    return (
      <aside className="sidebar flex-container flex-dir-column align-top">
        <header>
          <MenuLink to={`/dashboard/${props.user_id}`}>
            <img src={props.avatar} alt={fullName} /> {fullName}
          </MenuLink>
        </header>
        <SidebarContent />
      </aside>
    );
  }

  handleTouchStart = (e) => {
    touchStart = e.changedTouches[0].clientX;
  }

  handleTouchMove = (e) => {
    const newClientX = e.changedTouches[0].clientX;
    touchDirection = (newClientX > touchStart) ? 'right' : 'left';
    touchPosition = newClientX;
  }

  handleTouchEnd = (e) => {
    const { props } = this;

    if (props.isOpen) {
      props.updateMainNav({ isOpen: false });
      return;
    }

    if (touchDirection === 'right' && touchPosition > touchStart + 75) {
      props.updateMainNav({ isOpen: true });
      touchDirection = null;
    }
  }
}

const mapStateToProps = ({ ui, user }) => ({
  ...ui.mainnav,
  first_name: user.first_name,
  last_name: user.last_name,
  user_id: user._id,
  avatar: user.avatar
});

const mapDispatchToProps = (dispatch) => (
  bindActionCreators({ updateMainNav }, dispatch)
);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Sidebar);

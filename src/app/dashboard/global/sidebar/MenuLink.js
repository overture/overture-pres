import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';

import { updateMainNav } from '../../../actions/uiActions';

class MenuLink extends React.PureComponent {
  render() {
    const { props } = this;
    return <Link onClick={this.handleMenuClick} to={props.to}>{props.children}</Link>;
  }

  handleMenuClick = () => {
    this.props.updateMainNav({
      isOpen: false
    });

    if (typeof this.props.onChange === 'function') {
      this.props.onChange();
    }
  }
}

/* TODO: FIXME: ts was complaining about removing this but it's not needed */
const mapStateToProps = ({ ui }) => ({
  ...ui.mainnav
});

const mapDispatchToProps = (dispatch) => (
  bindActionCreators({ updateMainNav }, dispatch)
);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MenuLink);
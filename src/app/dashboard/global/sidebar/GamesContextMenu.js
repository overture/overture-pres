import React from 'react';
import _ from 'lodash';

import { store } from 'utils/configStore';
import { RequestGameList } from 'app/services/GameService';

import { Select } from 'components';

class GamesContextMenu extends React.PureComponent {
  state = {
    filters: {
      categories: [],
      mechanics: [],
      min_duration: null,
      max_duration: null,
      owned_by: [],
      weight: ''
    },
    includes: [],
    sorts: {}
  };

  render() {
    const { state } = this;

    return (
      <div className="sidebar-page__context">
        <h3 className="sidebar-heading--submenu">Sort Games <i className="fa fa-sort-amount-asc" /></h3>
        <ul className="no-bullets menu vertical">
          <label>
            Sort Results By
            <Select
              name="hot"
              onChange={this.handleSelectChange}
              options={[
                { label: 'What\'s Hot', value: 'hot' },
                { label: 'Min. Duration', value: 'min_duration' },
                { label: 'Max. Duration', value: 'max_duration' },
                { label: 'Min. Players', value: 'min_players' },
                { label: 'Max. Players', value: 'max_players' },
                { label: 'Weight', value: 'weight' },
                { label: 'BGG Ranking', value: 'bgg_ranking' }
              ]}
              placeholder="Any"
              value={state.sorts}
            />
          </label>
        </ul>
        <h3 className="sidebar-heading--submenu">Filter Games <i className="fa fa-sliders" /></h3>
        <label>
          Owned By
          <Select
            multi={true}
            name="owned_by"
            onChange={this.handleSelectChange}
            options={[
              { label: 'Trevor Pierce', value: '593a2c245ee35676febbbf9e' },
              { label: 'Luke Skywalker', value: '593a2c465ee35676febbbfa1' }
            ]}
            placeholder="Any"
            value={state.filters.owned_by}
          />
        </label>

        <div className="grid-x grid-margin-x">
          <div className="cell small-6">
            <label>
              Min Duration
              <Select
                name="min_duration"
                onChange={this.handleSelectChange}
                options={[
                  { label: '5', value: '5' },
                  { label: '20', value: '20' },
                  { label: '60', value: '60' },
                  { label: '90', value: '90' },
                  { label: '120', value: '120' }
                ]}
                placeholder="Any"
                value={state.filters.min_duration}
              />
            </label>
          </div>
          <div className="cell small-6">
            <label>
              Max Duration
              <Select
                name="max_duration"
                onChange={this.handleSelectChange}
                options={[
                  { label: '15', value: '15' },
                  { label: '45', value: '45' },
                  { label: '90', value: '90' },
                  { label: '120', value: '120' }
                ]}
                placeholder="Any"
                value={state.filters.max_duration}
              />
            </label>
          </div>
        </div>

        <label>
          Complexity <i className="fa fa-question-circle" />
          <Select
            name="weight"
            onChange={this.handleSelectChange}
            options={[
              { label: 'Super Light', value: 'superlight' },
              { label: 'Light', value: 'light' },
              { label: 'Medium', value: 'medium' },
              { label: 'Heavy', value: 'heavy' }
            ]}
            placeholder="Any"
            value={state.filters.weight}
          />
        </label>

        <Select
          label={<span>Mechanics <i className="fa fa-question-circle" /></span>}
          multi={true}
          name="mechanics"
          onChange={this.handleSelectChange}
          options={mechanics}
          placeholder="Any"
          value={state.filters.mechanics}
        />

        <label>
          <Select
            label={<span>Categories <i className="fa fa-question-circle" /></span>}
            multi={true}
            name="categories"
            onChange={this.handleSelectChange}
            options={categories}
            placeholder="Any"
            value={state.filters.categories}
          />
        </label>
      </div>
    );
  }

  handleSelectChange = (selectedOption, name) => {
    let update = (!selectedOption)
      ? { [name]: null }
      : {
        [name]: (
          _.isArray(selectedOption)
        ) ? selectedOption.map((o) => o.value)
          : selectedOption.value
      };

    this.setState({
      filters: _.extend({}, this.state.filters, update)
    }, () => {
      this.handleSearch();
    });
  }

  handleSearch() {
    RequestGameList()
      // .then(payload => store.dispatch(
      //   setGames(payload)
      // ))
      .catch((error) => console.error(error));
  }
}

export default GamesContextMenu;

const mechanics = [
  {
    'label': 'Acting',
    'value': 'acting'
  },
  {
    'label': 'Action / Movement Programming',
    'value': 'action_movement_programming'
  },
  {
    'label': 'Action Point Allowance System',
    'value': 'action_point_allowance_system'
  },
  {
    'label': 'Area Control / Area Influence',
    'value': 'area_control_area_influence'
  },
  {
    'label': 'Area Enclosure',
    'value': 'area_enclosure'
  },
  {
    'label': 'Area Movement',
    'value': 'area_movement'
  },
  {
    'label': 'Area-Impulse',
    'value': 'area_impulse'
  },
  {
    'label': 'Auction/Bidding',
    'value': 'auction_bidding'
  },
  {
    'label': 'Betting/Wagering',
    'value': 'betting_wagering'
  },
  {
    'label': 'Campaign / Battle Card Driven',
    'value': 'campaign_battle_card_driven'
  },
  {
    'label': 'Card Drafting',
    'value': 'card_drafting'
  },
  {
    'label': 'Chit-Pull System',
    'value': 'chit_pull_system'
  },
  {
    'label': 'Co-operative Play',
    'value': 'co_operative_play'
  },
  {
    'label': 'Commodity Speculation',
    'value': 'commodity_speculation'
  },
  {
    'label': 'Crayon Rail System',
    'value': 'crayon_rail_system'
  },
  {
    'label': 'Deck / Pool Building',
    'value': 'deck_pool_building'
  },
  {
    'label': 'Dice Rolling',
    'value': 'dice_rolling'
  },
  {
    'label': 'Grid Movement',
    'value': 'grid_movement'
  },
  {
    'label': 'Hand Management',
    'value': 'hand_management'
  },
  {
    'label': 'Hex-and-Counter',
    'value': 'hex_and_counter'
  },
  {
    'label': 'Line Drawing',
    'value': 'line_drawing'
  },
  {
    'label': 'Memory',
    'value': 'memory'
  },
  {
    'label': 'Modular Board',
    'value': 'modular_board'
  },
  {
    'label': 'Paper-and-Pencil',
    'value': 'paper_and_pencil'
  },
  {
    'label': 'Partnerships',
    'value': 'partnerships'
  },
  {
    'label': 'Pattern Building',
    'value': 'pattern_building'
  },
  {
    'label': 'Pattern Recognition',
    'value': 'pattern_recognition'
  },
  {
    'label': 'Pick-up and Deliver',
    'value': 'pick_up_and_deliver'
  },
  {
    'label': 'Player Elimination',
    'value': 'player_elimination'
  },
  {
    'label': 'Point to Point Movement',
    'value': 'point_to_point_movement'
  },
  {
    'label': 'Press Your Luck',
    'value': 'press_your_luck'
  },
  {
    'label': 'Rock-Paper-Scissors',
    'value': 'rock_paper_scissors'
  },
  {
    'label': 'Role Playing',
    'value': 'role_playing'
  },
  {
    'label': 'Roll / Spin and Move',
    'value': 'roll_spin_and_move'
  },
  {
    'label': 'Route/Network Building',
    'value': 'route_network_building'
  },
  {
    'label': 'Secret Unit Deployment',
    'value': 'secret_unit_deployment'
  },
  {
    'label': 'Set Collection',
    'value': 'set_collection'
  },
  {
    'label': 'Simulation',
    'value': 'simulation'
  },
  {
    'label': 'Simultaneous Action Selection',
    'value': 'simultaneous_action_selection'
  },
  {
    'label': 'Singing',
    'value': 'singing'
  },
  {
    'label': 'Stock Holding',
    'value': 'stock_holding'
  },
  {
    'label': 'Storytelling',
    'value': 'storytelling'
  },
  {
    'label': 'Take That',
    'value': 'take_that'
  },
  {
    'label': 'Tile Placement',
    'value': 'tile_placement'
  },
  {
    'label': 'Time Track',
    'value': 'time_track'
  },
  {
    'label': 'Trading',
    'value': 'trading'
  },
  {
    'label': 'Trick-taking',
    'value': 'trick_taking'
  },
  {
    'label': 'Variable Phase Order',
    'value': 'variable_phase_order'
  },
  {
    'label': 'Variable Player Powers',
    'value': 'variable_player_powers'
  },
  {
    'label': 'Voting',
    'value': 'voting'
  },
  {
    'label': 'Worker Placement',
    'value': 'worker_placement'
  }
];

const categories = [
  {
    'label': 'Board Game Categories',
    'value': 'board_game_categories'
  },
  {
    'label': 'Abstract Strategy',
    'value': 'abstract_strategy'
  },
  {
    'label': 'Action / Dexterity',
    'value': 'action_dexterity'
  },
  {
    'label': 'Adventure',
    'value': 'adventure'
  },
  {
    'label': 'Age of Reason',
    'value': 'age_of_reason'
  },
  {
    'label': 'American Civil War',
    'value': 'american_civil_war'
  },
  {
    'label': 'American Indian Wars',
    'value': 'american_indian_wars'
  },
  {
    'label': 'American Revolutionary War',
    'value': 'american_revolutionary_war'
  },
  {
    'label': 'American West',
    'value': 'american_west'
  },
  {
    'label': 'Ancient',
    'value': 'ancient'
  },
  {
    'label': 'Animals',
    'value': 'animals'
  },
  {
    'label': 'Arabian',
    'value': 'arabian'
  },
  {
    'label': 'Aviation / Flight',
    'value': 'aviation_flight'
  },
  {
    'label': 'Bluffing',
    'value': 'bluffing'
  },
  {
    'label': 'Book',
    'value': 'book'
  },
  {
    'label': 'Card Game',
    'value': 'card_game'
  },
  {
    'label': 'Children\'s Game',
    'value': 'childrens_game'
  },
  {
    'label': 'City Building',
    'value': 'city_building'
  },
  {
    'label': 'Civil War',
    'value': 'civil_war'
  },
  {
    'label': 'Civilization',
    'value': 'civilization'
  },
  {
    'label': 'Collectible Components',
    'value': 'collectible_components'
  },
  {
    'label': 'Comic Book / Strip',
    'value': 'comic_book_strip'
  },
  {
    'label': 'Deduction',
    'value': 'deduction'
  },
  {
    'label': 'Dice',
    'value': 'dice'
  },
  {
    'label': 'Economic',
    'value': 'economic'
  },
  {
    'label': 'Educational',
    'value': 'educational'
  },
  {
    'label': 'Electronic',
    'value': 'electronic'
  },
  {
    'label': 'Environmental',
    'value': 'environmental'
  },
  {
    'label': 'Expansion for Base-game',
    'value': 'expansion_for_base_game'
  },
  {
    'label': 'Exploration',
    'value': 'exploration'
  },
  {
    'label': 'Fan Expansion',
    'value': 'fan_expansion'
  },
  {
    'label': 'Fantasy',
    'value': 'fantasy'
  },
  {
    'label': 'Farming',
    'value': 'farming'
  },
  {
    'label': 'Fighting',
    'value': 'fighting'
  },
  {
    'label': 'Game System',
    'value': 'game_system'
  },
  {
    'label': 'Horror',
    'value': 'horror'
  },
  {
    'label': 'Humor',
    'value': 'humor'
  },
  {
    'label': 'Industry / Manufacturing',
    'value': 'industry_manufacturing'
  },
  {
    'label': 'Korean War',
    'value': 'korean_war'
  },
  {
    'label': 'Mafia',
    'value': 'mafia'
  },
  {
    'label': 'Math',
    'value': 'math'
  },
  {
    'label': 'Mature / Adult',
    'value': 'mature_adult'
  },
  {
    'label': 'Maze',
    'value': 'maze'
  },
  {
    'label': 'Medical',
    'value': 'medical'
  },
  {
    'label': 'Medieval',
    'value': 'medieval'
  },
  {
    'label': 'Memory',
    'value': 'memory'
  },
  {
    'label': 'Miniatures',
    'value': 'miniatures'
  },
  {
    'label': 'Modern Warfare',
    'value': 'modern_warfare'
  },
  {
    'label': 'Movies / TV / Radio theme',
    'value': 'movies_tv_radio_theme'
  },
  {
    'label': 'Murder/Mystery',
    'value': 'murdermystery'
  },
  {
    'label': 'Music',
    'value': 'music'
  },
  {
    'label': 'Mythology',
    'value': 'mythology'
  },
  {
    'label': 'Napoleonic',
    'value': 'napoleonic'
  },
  {
    'label': 'Nautical',
    'value': 'nautical'
  },
  {
    'label': 'Negotiation',
    'value': 'negotiation'
  },
  {
    'label': 'Novel-based',
    'value': 'novel_based'
  },
  {
    'label': 'Number',
    'value': 'number'
  },
  {
    'label': 'Party Game',
    'value': 'party_game'
  },
  {
    'label': 'Pike and Shot',
    'value': 'pike_and_shot'
  },
  {
    'label': 'Pirates',
    'value': 'pirates'
  },
  {
    'label': 'Political',
    'value': 'political'
  },
  {
    'label': 'Post-Napoleonic',
    'value': 'post_napoleonic'
  },
  {
    'label': 'Prehistoric',
    'value': 'prehistoric'
  },
  {
    'label': 'Print & Play',
    'value': 'print_play'
  },
  {
    'label': 'Puzzle',
    'value': 'puzzle'
  },
  {
    'label': 'Racing',
    'value': 'racing'
  },
  {
    'label': 'Real-time',
    'value': 'real_time'
  },
  {
    'label': 'Religious',
    'value': 'religious'
  },
  {
    'label': 'Renaissance',
    'value': 'renaissance'
  },
  {
    'label': 'Science Fiction',
    'value': 'science_fiction'
  },
  {
    'label': 'Space Exploration',
    'value': 'space_exploration'
  },
  {
    'label': 'Spies/Secret Agents',
    'value': 'spies_secret_agents'
  },
  {
    'label': 'Sports',
    'value': 'sports'
  },
  {
    'label': 'Territory Building',
    'value': 'territory_building'
  },
  {
    'label': 'Trains',
    'value': 'trains'
  },
  {
    'label': 'Transportation',
    'value': 'transportation'
  },
  {
    'label': 'Travel',
    'value': 'travel'
  },
  {
    'label': 'Trivia',
    'value': 'trivia'
  },
  {
    'label': 'Video Game Theme',
    'value': 'video_game_theme'
  },
  {
    'label': 'Vietnam War',
    'value': 'vietnam_war'
  },
  {
    'label': 'Wargame',
    'value': 'wargame'
  },
  {
    'label': 'Word Game',
    'value': 'word_game'
  },
  {
    'label': 'World War I',
    'value': 'world_war_i'
  },
  {
    'label': 'World War II',
    'value': 'world_war_ii'
  },
  {
    'label': 'Zombies',
    'value': 'zombies'
  }
];
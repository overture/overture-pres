import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { RequestGameList } from 'app/services';
import GamesContextMenu from './GamesContextMenu';
import GroupsMenu from './GroupsMenu';
import MenuLink from './MenuLink';

import { updateMainNav, updateGlobalModals } from 'app/actions/uiActions';

const factor = 33.33333;
let touchDirection = null;
let touchStart = 0;

class SidebarContent extends React.Component {
  state = {
    current: 1
  };

  componentDidMount() {
    // We need to check if we are mobile or not
    if (true) {
      const $sidebarContent = document.getElementById('sidebar-content');

      $sidebarContent.addEventListener('touchstart', this.handleTouchStart);
      $sidebarContent.addEventListener('touchmove', this.handleTouchMove);
      $sidebarContent.addEventListener('touchend', this.handleTouchEnd);
    }
  }

  componentWillUnmount() {
    const $sidebarContent = document.getElementById('main-content');

    $sidebarContent.removeEventListener('touchstart', this.handleTouchStart);
    $sidebarContent.removeEventListener('touchmove', this.handleTouchMove);
    $sidebarContent.removeEventListener('touchend', this.handleTouchEnd);
  }

  render() {
    const { props, state } = this;

    return (
      <div
        id="sidebar-content"
        className="sidebar__content"
        style={{
          transform: 'translateX(-' + (state.current * factor) + '%) translateZ(0)'
        }}
      >
        <GroupsMenu groups={[]} />

        {/* Main Menu */}
        <ul className="sidebar-page__main no-bullet menu vertical">
          <li>
            <MenuLink to={`/dashboard/games`}>
              Games
            </MenuLink>
          </li>
          <li>
            <MenuLink
              onChange={e => this.handleRequestGames({
                owned_by: [props.user_id]
              })}
              to={`/dashboard/games/collection/${props.user_id}`}
            >
              My Collection
            </MenuLink>
          </li>
          <li><MenuLink to="/dashboard/log-play">Log Play</MenuLink></li>
          <li><a onClick={() => props.updateGlobalModals('createEvent')}>Create Event</a></li>
          <li><MenuLink to="/dashboard/events?location=75071">Public Events</MenuLink></li>
          <li><MenuLink to={`/dashboard/${props.user_id}/groups`}>My Groups</MenuLink></li>
          <li><MenuLink to="/dashboard/lfg">Looking for Group</MenuLink></li>
        </ul>

        <GamesContextMenu />
      </div>
    );
  }

  handleTouchStart = (e) => {
    touchStart = e.changedTouches[0].clientX;
  }

  handleTouchMove = (e) => {
    const newClientX = e.changedTouches[0].clientX;
    touchDirection = (newClientX > touchStart) ? 'right' : 'left';
  }

  handleTouchEnd = (e) => {
    const { state } = this;
    const touch = e.changedTouches[0];
    const sideTouchEnd = touch.clientX;

    if (touchDirection === 'right' && (sideTouchEnd > touchStart + 30)) {
      this.setState({
        current: (state.current === 0) ? 0 : state.current - 1
      });
    }

    if (touchDirection === 'left' && (sideTouchEnd < touchStart - 30)) {
      this.setState({
        current: (state.current === 2) ? 2 : state.current + 1
      });
    }
  }

  handleRequestGames = params => {
    const { props } = this;

    RequestGameList(params)
      // .then(payload => props.setGames(payload))
      .catch((error) => console.error(error));
  }
}

const mapStateToProps = ({ ui, user }) => ({
  ...ui.mainnav,
  user_id: user._id
});

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    updateMainNav,
    updateGlobalModals,
  }, dispatch)
);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SidebarContent);

import * as React from 'react';
import { Link } from 'react-router-dom';

export default (({ groups }) => {
  let groupsBody = (
    <div>
      You are not currently in any groups.
      <ul className="no-bullet menu vertical">
        <li><Link to="/dashboard/lfg">Join a group</Link></li>
        <li><Link to="/dashboard/groups/create">Create a group</Link></li>
      </ul>
    </div>
  );

  if (groups.length) {
    groupsBody = (
      groups.map((group) => (
        <li className="group-link flex-container">
          <Link to={`/groups/user_id/group_id`}>
            <img src={group.logo} alt={group.name} /> {group.name}
          </Link>
        </li>
      ))
    );
  }

  return (
    <div className="sidebar-page__groups">
      <ul className="no-bullet menu vertical">{groupsBody}</ul>
    </div>
  );
});

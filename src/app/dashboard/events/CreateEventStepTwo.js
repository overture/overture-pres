import React from 'react';
import classnames from 'classnames';
import { Select } from 'components';

export default props => {
  const modalClasses = classnames(
    'modal', {
      'is-active': props.isActive
    }
  );
  let inlineStyles = {};

  if (props.xOffset) {
    inlineStyles = {
      transform: `translate(${props.xOffset}%, -50%) scale(${props.isActive ? 1 : .95})`
    };
  }

  return (
    <div className={modalClasses} style={inlineStyles}>
      <h2>Create Game Night</h2>
      <h3>Attendees</h3>
      <Select
        label="Invite Group"
        name="invite_group"
        options={[
          { label: 'The McKinney Knights', value: '12345' }
        ]}
      />
      <Select
        label="Invite People"
        name="invite_people"
        options={[
          { label: 'Trevor Pierce', value: '12345' },
          { label: 'Luke Skywalker', value: '67890' },
        ]}
      />

      <h4>Attendees</h4>
      <ul className="callout secondary no-bullet">
        <li className="flex-container align-justify align-middle">
          <span>Trevor Pierce</span>
          <Select
            options={[
              { label: 'confirmed', value: 'confirmed' }
            ]}
          />
        </li>
      </ul>

      <footer className="modal__footer align-justify">
        <button onClick={props.prevStep}>Back: Date & Time</button>
        <button className="button primary" onClick={props.nextStep}>Next: Location</button>
      </footer>

      <button className="modal-close" onClick={props.onRequestClose}>
        <span aria-hidden className="is-hidden">close modal</span>
        <i className="fa fa-times-circle-o" />
      </button>
    </div>
  );
}

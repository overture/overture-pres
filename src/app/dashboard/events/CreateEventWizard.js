import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { updateGlobalModals } from 'app/actions/uiActions';
import {
  CreateEventStepOne,
  CreateEventStepTwo
} from 'app/dashboard/events';

class CreateEventWizard extends React.Component {
  translateBase = -50;
  state = {
    currentStep: 1,
    newEvent: {},
    stepTemp: {},
    progress: {}
  };

  constructor(props) {
    super(props);

    this.onRequestClose = this.onRequestClose.bind(this);
    this.nextStep = this.nextStep.bind(this);
    this.prevStep = this.prevStep.bind(this);
  }

  render() {
    const { props, state } = this;

    return (
      <div className="multi-modal modal-overlay modal-overlay--after-open">
        <CreateEventStepOne
          key="create-date-time-step"
          newEvent={state.newEvent}
          onRequestClose={this.onRequestClose}
          nextStep={this.nextStep}
          prevStep={this.prevStep}
          xOffset={!props.isMobile ? this.calculateOffset(1) : null}
          isActive={state.currentStep === 1}
        />

        <CreateEventStepTwo
          key="create-attendees-step"
          newEvent={state.newEvent}
          onRequestClose={this.onRequestClose}
          nextStep={this.nextStep}
          prevStep={this.prevStep}
          xOffset={!props.isMobile ? this.calculateOffset(2) : null}
          isActive={state.currentStep === 2}
        />
      </div>
    );
  }

  calculateOffset(step) {
    const { state } = this;
    const start = this.translateBase;
    const multiplier = step < state.currentStep ? -110 : 110;

    if (step === state.currentStep) {
      return start;
    }

    // determines positions on the left / right side of active
    return step < state.currentStep
      ? start + ((state.currentStep - step) * multiplier)
      : start + ((step - state.currentStep) * multiplier);
  }

  nextStep() {
    const { state } = this;
    this.setState({ currentStep: state.currentStep + 1 });
  }

  prevStep() {
    const { state } = this;
    this.setState({ currentStep: state.currentStep - 1 });
  }

  onRequestClose() {
    const { props } = this;
    // clear local
    this.setState({ stepTemp: {} });
    // wipe all store data
    props.history.goBack();
  }

  updateNewEvent = (update) => {
    const { newEvent } = this.state;
    this.setState({ newEvent: { ...newEvent, ...update } });
  }
}

const mapStateToProps = ({ ui }) => ({
  isMobile: ui.isMobile
});

const mapDispatchToProps = (dispatch) => (
  bindActionCreators({ updateGlobalModals }, dispatch)
);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateEventWizard);

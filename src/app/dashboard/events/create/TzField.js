import React from 'react';
import { connect } from 'react-redux';
import classnames from 'classnames';

import { Select } from 'components';

const TzWarningNotice = props => {
  return (
    !props.isTZMatch
      ? <div className="callout--enhanced warning">
          <p>We noticed the current timezone in your browser is different than the one in your profile. Please confirm the timezone for this event.</p>
        </div>
      : null
  );
}

class TzField extends React.PureComponent {
  render() {
    const { props } = this;
    const isTZMatch = props.browserTimezone === props.userTimezone;
    const tzSelectClasses = classnames({ 'has-warning': !isTZMatch });

    return ([
      <Select
        className={tzSelectClasses}
        label="Timezone"
        name="tz"
        options={[
          { label: 'America/Chicago', value: 'America/Chicago' }
        ]}
        value={props.userTimezone || props.browserTimezone}
      />,
      <TzWarningNotice isTZMatch={isTZMatch} />
    ]);
  }
}

const mapStateToProps = ({ ui, user }) => ({
  browserTimezone: ui.browserTimezone,
  userTimezone: user.tz
});

export default connect(
  mapStateToProps
)(TzField);
import React from 'react';
import classnames from 'classnames';
import DatePicker from 'components/pickers/DatePicker';
import TimePicker from 'components/pickers/TimePicker';
import { Select } from 'components';

export default props => {
  const modalClasses = classnames(
    'modal', {
      'is-active': props.isActive
    }
  );
  let inlineStyles = {};

  if (props.xOffset) {
    inlineStyles = {
      transform: `translate(${props.xOffset}%, -50%) scale(${props.isActive ? 1 : .95})`
    };
  }

  return (
    <div className={modalClasses} style={inlineStyles}>
      <header className="modal__header">
        <h2>Create Game Night</h2>
        <h3>Date & Time</h3>
      </header>
      <DatePicker
        label="Event Date"
        name="from"
        required
      />
      <div className="grid-x grid-margin-x">
        <div className="cell small-6">
          <TimePicker
            label="Start Time"
            name="start"
            onChange={(value, name) => props.updateNewEvent({
              [name]: value
            })}
            required
            value={props.newEvent.startTime}
          />
        </div>
        <div className="cell small-6">
          <TimePicker
            label="End Time"
            name="end"
            onChange={(value, name) => props.updateNewEvent({
              [name]: value
            })}
            required
            value={props.newEvent.endTime}
          />
        </div>
      </div>
      <Select
        label="Timezone"
        name="tz"
        options={[
          { label: 'America/Chicago', value: 'america/chicago' }
        ]}
      />
      <div className="callout--enhanced warning">
        <p>We noticed the current timezone in your browser is different than the one in your profile. Please confirm the timezone for this event.</p>
      </div>
      <hr/>
      <div><h3>Reoccurance Settings</h3></div>

      <footer className="modal__footer align-right">
        <button className="button primary" onClick={props.nextStep}>Next: Attendees</button>
      </footer>

      <button className="modal-close" onClick={props.onRequestClose}>
        <span aria-hidden className="is-hidden">close modal</span>
        <i className="fa fa-times-circle-o" />
      </button>
    </div>
  );
}

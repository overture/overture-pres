import Api from '../../utils/configAxios';

export const PostPlay = (payload) => {
  return new Promise((resolve, reject) => {
    Api
      .post('/plays', payload)
      .then((response) => resolve(response.data))
      .catch((error) => {
        console.log('error saving play log: ', error);
        reject(error);
      });
  });
};

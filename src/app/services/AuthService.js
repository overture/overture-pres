import Api from '../../utils/configAxios';
import { setUser } from 'app/reducers/userReducer';

export const RequestLogin = (
  credentials
) => {
  return (dispatch) => {
    Api
      .post('/authenticate', credentials)
      .then(({ data }) => {
        return (
          Api
            .get(`/users/${data.data.id}`)
            .then(userPayload => {
              dispatch(setUser(userPayload.data.data));
            })
            .catch((error) => Promise.reject(error))
        );
      })
      .catch((error) => {
        // set some errors in the state...
        console.log('something went wrong: ', error);
      });
  };
};

// export const RequestSignup = (payload: {}) => {};
// export const RequestForgotPassword = (payload: {}) => {};

import qs from 'qs';
import Api from 'utils/configAxios';
import { ServiceHandlerFactory } from 'utils/serviceUtils';

export const RequestGameList = ServiceHandlerFactory({
  axios: {
    url: '/games',
    paramsSerializer: _params => qs.stringify(_params)
  },
  isCancelable: true,
})

export const RequestGame = (id) => {
  return new Promise((resolve, reject) => {
    Api
      .get(`/games/${id}`)
      .then((payload) => resolve(payload.data))
      .catch((error) => {
        console.log('error getting game: ', error);
        reject(error);
      });
  });
};

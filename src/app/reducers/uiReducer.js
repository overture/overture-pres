import {
  SET_LOADING,
  SET_CONTEXT,
  UPDATE_MAIN_NAV,
  UPDATE_CONTEXT_NAV,
  UPDATE_GLOBAL_MODALS
} from '../actions';

const initialState = {
  isLoading: false,
  isMobile: window.outerWidth < 480,
  mainnav: {
    isOpen: false
  },
  contextNav: {
    isOpen: false
  },
  modals: {
    createEvent: false,
    logPlay: false
  }
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_LOADING:
      return { ...state, ...action.payload };

    case UPDATE_MAIN_NAV:
      const mainnav = { ...state.mainnav, ...action.payload };
      return { ...state, mainnav };

    case UPDATE_CONTEXT_NAV:
      return { ...state, contextNav: action.payload };

    case UPDATE_GLOBAL_MODALS:
      const modals = (
        Object.keys(state.modals).reduce((acc, key) => {
          acc[key] = (key === action.payload);
          return acc;
        }, {})
      );
      return { ...state, modals };

    case SET_CONTEXT:
      return { ...state, context: action.payload };

    default:
      return state;
  }
};

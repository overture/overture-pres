// action types
export const LOGOUT = 'LOGOUT';
export const SET_AUTHENTICATED = 'SET_AUTHENTICATED';
export const SET_USER = 'SET_USER';

// actions
export const logout = () => ({
  type: LOGOUT,
})

export const setUserAuthenticated = (payload) => ({
  type: SET_AUTHENTICATED,
  payload
});

export const setUser = (payload) => ({
  type: SET_USER,
  payload
});

// reducer
const initialState = {
  isAuthenticated: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_AUTHENTICATED:
      return { ...state, ...action.payload };

    case SET_USER:
      return { ...state, ...action.payload };

    case LOGOUT:
      return initialState;

    default:
      return state;
  }
};

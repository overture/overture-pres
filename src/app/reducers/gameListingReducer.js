import {
  SET_GAME_LISTING_FILTERS,
  CLEAR_GAME_LISTING_FILTERS
} from '../actions';

const initialState = {
  games: [],
  filters: {},
};

export default (state = initialState, action) => {
  switch (action.type) {

    case SET_GAME_LISTING_FILTERS:
      let newFilterState = { ...state };
      newFilterState.filters = { ...state.filters, ...action.payload };
      return newFilterState;

    case CLEAR_GAME_LISTING_FILTERS:
      let stateCopy = { ...state };
      stateCopy.filters = {};
      return stateCopy;

    default:
      return state;
  }
};

import { combineReducers } from 'redux';
import { ui, user, gameListing } from './';
import gamesCache from '../dashboard/games/reducers/gamesCache.reducer';

export default combineReducers({
  gameListing,
  gamesCache,
  ui,
  user
});

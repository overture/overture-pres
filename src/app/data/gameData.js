export const availableGameInteractions = [
  {
    label: 'Competitive',
    value: 'competitive'
  },
  {
    label: 'Cooperative',
    value: 'coop'
  },
  {
    label: 'Semi-Coopoerative',
    value: 'semi-coop'
  },
  {
    label: 'Solo',
    value: 'coop'
  }
];

import  React from 'react';
import { Provider } from 'react-redux';
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom';

import { store } from './utils/configStore';
import './config';

// components
import Login from 'app/login/Login';
import Logout from 'app/login/Logout';
import Signup from 'app/login/Signup';
import ForgotPassword from 'app/login/ForgotPassword';
import ResetPassword from 'app/login/ResetPassword';
import DashboardWrapper from 'app/dashboard/DashboardWrapper';
import DashboardRoute from 'app/dashboard/routes/DashboardRoute';
import NotFound from './NotFound';

import './assets/scss/main.css';

const App = () => (
  <Provider store={store}>
    <Router>
      <Switch>
        <Route path="/login" component={Login} />
        <Route path="/logout" component={Logout}/>
        <Route path="/signup" component={Signup} />
        <Route exact path="/forgot" component={ForgotPassword} />
        <Route path="/forgot/:token" component={ResetPassword} />
        <DashboardRoute path="/dashboard" component={DashboardWrapper} />
        <Route path={`/*`} component={NotFound} />
      </Switch>
    </Router>
  </Provider>
);

export default App;

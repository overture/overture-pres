import * as React from 'react';
import { Component } from 'react';
import * as ReactModal from 'react-modal';
import * as classnames from 'classnames';

class Modal extends Component {
  render() {
    const { props } = this;
    const baseClasses = classnames('modal', props.className);

    return (
      <ReactModal
        className={{
          base: baseClasses,
          afterOpen: '',
          beforeClose: ''
        }}
        overlayClassName={{
          base: 'modal-overlay',
          afterOpen: 'modal-overlay--after-open',
          beforeClose: ''
        }}
        ariaHideApp={false}
        contentLabel={props.contentLabel}
        isOpen={props.isOpen}
        onAfterOpen={props.onAfterOpen}
        onRequestClose={props.onRequestClose}
      >
        {props.children}
        <button className="modal-close" onClick={props.onRequestClose}>
          <span aria-hidden className="is-hidden">close modal</span>
          <i className="fa fa-times-circle-o" />
        </button>
      </ReactModal>
    );
  }
}

export default Modal;

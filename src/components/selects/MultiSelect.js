import React from 'react';
import { isFunction, find, pull } from 'lodash';
import Select from './Select';

class MultiSelect extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      values: props.defaultValues,
      isLoading: false,
    };

    this.onChange = this.onChange.bind(this);
  }

  render() {
    const { props, state } = this;

    return (
      <Select
        {...props}
        multi
        value={state.values}
        onChange={this.onChange}
      />
    );
  }

  onChange(value) {
    const { props, state } = this;
    let values = (
      state.values.indexOf(value) !== -1
        ? pull(state.values, value)
        : [...state.values, value]
    );

    if (value === 'any') {
      values = props.defaultValues;
    }

    if (isFunction(props.onChange)) {
      props.onChange(values, props.name);
    }

    this.setState({ values });
  }
}

MultiSelect.defaultProps = {
  defaultValues: [],
};

export default MultiSelect;

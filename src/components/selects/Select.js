import React from 'react';
import { connect } from 'react-redux';
import Select from 'react-select';

const MultiSelect = props => {
  return (
    props.isMobile ? (
      <label>
        {props.label}
        <select
          className={props.className}
          multiple={props.multi}
          onChange={_changeHandler(props, props.isMobile)}
          value={props.value}
        >
          {props.options.map(opt => {
            return (
              <option
                key={`${props.name}-${opt.value}`}
                placeholder={props.placeholder}
                value={opt.value}
              >
                {opt.label}
              </option>
            );
          })}
        </select>
      </label>
    ) : (
        <label>
          {props.label}
          <Select
            {...props}
            onChange={_changeHandler(props, props.isMobile)}
          />
        </label>
      )
  );
}

function _changeHandler(props, isNative) {
  if (typeof props.onChange !== 'function') {
    return null;
  }

  return function(e) {
    let value = isNative ? e.target.value : e;

    if (!value || value === 'any') {
      value = 'any';
    }

    props.onChange(value, props.name);
  };
}

MultiSelect.defaultProps = {
  value: '',
};

const mapStateToProps = ({ ui }) => ({
  isMobile: ui.isMobile
});

export default connect(
  mapStateToProps
)(MultiSelect);

import { extend } from 'lodash';
import qs from 'qs';
import React from 'react';
import { Route } from 'react-router-dom';
import hoistStatics from 'hoist-non-react-statics';

let cache = {};
let cacheCount = 0;

export default function withRouter(Component) {
  const C = props => {
    const { wrappedComponentRef, ...rest } = props;

    return (
      <Route
        children={({ location, ...routeRest }) => {
          let query = {};

          if (location.search) {
            // pull out of cache if we can
            if (cache[location.search]) {
              query = cache[location.search];
            } else {
              query = qs.parse(location.search, { ignoreQueryPrefix: true });

              // place new query into cache
              cache[location.search] = query;
              cacheCount += 1;
            }

            // free up some memory
            if (cacheCount > 500) {
              cache = {};
            }
          }

          const modifiedProps = {
            ...routeRest,
            location: {
              ...location,
              query,
            },
          };

          return (
            <Component
              {...rest}
              {...modifiedProps}
              updateUrl={_updateUrl(query, routeRest.match, routeRest.history)}
              ref={wrappedComponentRef}
            />
          );
        }}
      />
    );
  };

  C.displayName = `withRouter(${Component.displayName || Component.name})`;
  C.wrappedComponent = Component;

  return hoistStatics(C, Component);
}

function _updateUrl(query, match, history) {
  return (update = {}, method = 'replace', pathnameOverride) => {
    const rawParams = extend({}, query, update);

    history[method]({
      pathname: pathnameOverride || match.url.pathname,
      search: qs.stringify(
        rawParams,
        {
          addQueryPrefix: true,
          encode: false,
        },
      ),
    });
  };
}

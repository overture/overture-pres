import * as React from 'react';
import { connect } from 'react-redux';
import ReactTimePicker from 'react-time-picker';

const TimePicker = (props) => {
  const { isMobile } = props;

  return isMobile ?
    (
      <label>
        {props.label}
        <input
          type="time"
          name={props.name}
          value={props.value}
          onChange={function (event) {
            const { value } = event.target;

            if (props.onChange) {
              props.onChange(value, props.name);
            }
          }}
          required={props.required}
        />
      </label>
    ) :
    (
      <label>
        {props.label}
        <ReactTimePicker
          {...props}
          onChange={function (value) {
            if (props.onChange) {
              props.onChange(value, props.name);
            }
          }}
        />
      </label>
    );
};

const mapStateToProps = ({ ui }) => ({
  isMobile: ui.isMobile
});

export default connect(
  mapStateToProps
)(TimePicker);

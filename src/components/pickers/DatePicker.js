import React from 'react';
import { connect } from 'react-redux';
import Moment from 'moment';
import ReactDatePicker from 'react-date-picker';

const DatePicker = (props) => {
  const { isMobile } = props;

  return isMobile ? (
    <label>
      {props.label}
      <input
        type="date"
        name={props.name}
        onChange={function (event) {
          const { value } = event.target;
          if (props.onChange) {
            props.onChange(
              Moment(value).toISOString(),
              props.name
            );
          }
        }}
        required={props.required}
      />
    </label>
  ) : (
    <label>
      {props.label}
      <ReactDatePicker
        {...props}
        onChange={function (value) {
          if (props.onChange) {
            props.onChange(
              Moment(value).toISOString(),
              props.name
            );
          }
        }}
      />
    </label>
  );
};

const mapStateToProps = ({ ui }) => ({
  isMobile: ui.isMobile
});

export default connect(
  mapStateToProps
)(DatePicker);

const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');

module.exports = function(env = {}) {
  const isProd = env.production;

  return {
    watch: !isProd,
    watchOptions: {
      ignored: /node_modules/
    },

    entry: {
      main: './src/assets/scss/App.scss'
    },

    output: {
      filename: (isProd) ? '[name].[chunkhash].js' : '[name].js',
      path: path.resolve(__dirname, (isProd) ? 'build' : 'src/assets/scss')
    },

    module: {
      loaders: [
        {
          test: /\.scss$/,
          use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: ['css-loader', 'sass-loader']
          })
        }
      ]
    },

    plugins: [
      new ManifestPlugin({
        fileName: 'css-manifest.json'
      }),

      new ExtractTextPlugin({
        filename: (isProd) ? '[name].[chunkhash].css' : '[name].css'
      })
    ]
  };
};
